import Vue from 'vue'{{#if_eq lintConfig "airbnb"}};{{/if_eq}}
import Router from 'vue-router'{{#if_eq lintConfig "airbnb"}};{{/if_eq}}
import Home from '@/components/Home'{{#if_eq lintConfig "airbnb"}};{{/if_eq}}
import Styleguide from '@/components/Styleguide'{{#if_eq lintConfig "airbnb"}};{{/if_eq}}

Vue.use(Router){{#if_eq lintConfig "airbnb"}};{{/if_eq}}

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home{{#if_eq lintConfig "airbnb"}},{{/if_eq}}
    },
    {
      path: '/styleguide',
      name: 'Styleguide',
      component: Styleguide{{#if_eq lintConfig "airbnb"}},{{/if_eq}}
    }{{#if_eq lintConfig "airbnb"}},{{/if_eq}}
  ]{{#if_eq lintConfig "airbnb"}},{{/if_eq}}
}){{#if_eq lintConfig "airbnb"}};{{/if_eq}}
